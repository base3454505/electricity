import { nanoid } from "nanoid";

export class World {
  workingPowPlant = [];

  constructor() {}

  createPowerPlant() {
    const id = nanoid();
    this.workingPowPlant.push({ id });
    return {
      id,
    };
  }

  createHousehold() {
    return {
      id: nanoid(),
      connectedPowerPlants: [],
      connectedHouseHouseholds: [],
    };
  }

  connectHouseholdToPowerPlant(household, powerPlant) {
    if (!this.workingPowPlant.some(({ id }) => id === powerPlant.id)) {
      return false;
    }
    household.connectedPowerPlants.push(powerPlant);
  }

  connectHouseholdToHousehold(household1, household2) {
    household1.connectedHouseHouseholds.push(household2);
  }

  disconnectHouseholdFromPowerPlant(household, powerPlant) {
    const index = household.connectedPowerPlants.findIndex(
      ({ id }) => id === powerPlant.id
    );
    household.connectedPowerPlants.splice(index, 1);
  }

  killPowerPlant(powerPlant) {
    const index = this.workingPowPlant.findIndex(
      ({ id }) => id === powerPlant.id
    );
    this.workingPowPlant.splice(index, 1);
  }

  repairPowerPlant(powerPlant) {
    if (!this.workingPowPlant.find(({ id }) => id === powerPlant.id)) {
      this.workingPowPlant.push(powerPlant);
    }
  }

  householdHasEletricity(household) {
    const powerPlants = household?.connectedPowerPlants;
    const households = household?.connectedHouseHouseholds;

    if (this.workingPowPlant.length == 0) {
      return false;
    }

    for (let i = 0; i < powerPlants.length; i++) {
      if (this.workingPowPlant.some(({ id }) => id === powerPlants[i].id)) {
        return true;
      }
    }

    if (households.length > 0) {
      for (let householdInHousehold of households) {
        return this.householdHasEletricity(householdInHousehold);
      }
    }

    return false;
  }
}
